#include "aludel.h"

#include "config.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QDBusConnection>

#include <QDebug>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    // get dunst out of the way
    system("pkill dunst");

    QCommandLineParser args;
    args.setSingleDashWordOptionMode(QCommandLineParser::ParseAsLongOptions);
    args.addOption(QCommandLineOption(QStringList() << "config" << "conf", "", "config path"));
    args.process(a);

    Config::configFilePath = args.value("config");

    auto bus = QDBusConnection::sessionBus();
    auto al = Aludel::Instance();
    if (!bus.registerService(QS("org.freedesktop.Notifications")) || !bus.registerObject(QS("/org/freedesktop/Notifications"), al)) {
        qFatal("Notification daemon already running; aborting");
    }

    return a.exec();
}
