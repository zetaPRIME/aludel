#include "config.h"

#include <QStandardPaths>
#include <QSettings>
#include <QTextCodec>

#include <QDebug>

using Config::Corner;

QString Config::configFilePath = { };

Corner Config::toastCorner = Corner::bottomRight;
QPoint Config::toastMargin = {8, 8};
int Config::toastSpacing = 3;

int Config::defaultTimeout = 5000;

void Config::init() {
    if (configFilePath.isEmpty()) configFilePath = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QStringLiteral("/aludel/aludel.conf");
    QSettings cfg(configFilePath, QSettings::IniFormat);
    cfg.setIniCodec("UTF-8");

    {
        auto c = cfg.value("toast-corner").toString();
        /**/ if (c == "top-left") toastCorner = Corner::topLeft;
        else if (c == "top-right") toastCorner = Corner::topRight;
        else if (c == "bottom-left") toastCorner = Corner::bottomLeft;
        else if (c == "bottom-right") toastCorner = Corner::bottomRight;
        else toastCorner = Corner::bottomRight;
    }
    toastMargin = { cfg.value("toast-margin-x", 8).toInt(), cfg.value("toast-margin-y", 8).toInt() };
    toastSpacing = cfg.value("toast-spacing", 3).toInt();
    defaultTimeout = cfg.value("default-timeout", 5000).toInt();
}

QPoint Config::cornerProportion() {
    switch(toastCorner) {
    case Corner::topLeft:
        return {0, 0};
    case Corner::topRight:
        return {1, 0};
    case Corner::bottomLeft:
        return {0, 1};
    case Corner::bottomRight:
        return {1, 1};
    }
    return {0, 0};
}

QPoint Config::cornerOffset() { return (cornerProportion() * 2 - QPoint(1, 1)) * 1; }

void Config::moveRectCorner(QRect &r, const QPoint &p) {
    switch(toastCorner) {
    case Corner::topLeft:
        r.moveTopLeft(p);
        return;
    case Corner::topRight:
        r.moveTopRight(p);
        return;
    case Corner::bottomLeft:
        r.moveBottomLeft(p);
        return;
    case Corner::bottomRight:
        r.moveBottomRight(p);
        return;
    }
}
