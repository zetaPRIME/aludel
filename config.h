#pragma once

#include <QPoint>
#include <QMargins>
#include <QRect>

namespace Config {
    enum class Corner {
        topLeft, topRight, bottomLeft, bottomRight
    };
    extern Corner toastCorner;
    extern QPoint toastMargin;
    extern int toastSpacing;

    extern int defaultTimeout;

    extern QString configFilePath;

    extern void init();
    extern QPoint cornerProportion();
    extern QPoint cornerOffset();

    extern void moveRectCorner(QRect& r, const QPoint& p);
}
