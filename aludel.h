#pragma once

#include <QObject>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QVariantMap>

#include "notification.h"

#define QS QStringLiteral

class Aludel : public QObject {
    Q_OBJECT

public:
    static Aludel* Instance();
    ~Aludel();
private:
    Aludel(QObject *parent = nullptr);

    uint nextId = 0;
    QMap<uint, Notification*> notifications;

    void updateGeometry();

public slots:
    QStringList GetCapabilities();
    QString GetServerInformation(QString& vendor, QString& version, QString& spec_version);
    uint Notify(const QString& app_name,
                uint replaces_id,
                const QString& app_icon,
                const QString& summary,
                const QString& body,
                const QStringList& actions,
                const QVariantMap& hints,
                int expire_timeout);
    void CloseNotification(uint id);

signals:
    void ActionInvoked(uint id, const QString& action);
    void NotificationClosed(uint id, uint reason);
};
