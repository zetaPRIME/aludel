#include "ui_notification.h"
#include "notification.h"

#include "config.h"

#include <QDesktopServices>
#include <QScreen>
#include <QDBusArgument>
#include <QTextDocument>
#include <QPushButton>
#include <QMouseEvent>

#include <QDebug>

namespace {
    QPixmap getImageFromHint(const QVariant& hint) {
        int width, height, rowstride, bitsPerSample, channels;
        bool hasAlpha;
        QByteArray data;

        const QDBusArgument header = hint.value<QDBusArgument>();
        header.beginStructure();
        header >> width >> height >> rowstride >> hasAlpha >> bitsPerSample >> channels >> data;
        header.endStructure();

        bool rgb = !hasAlpha && channels == 3 && bitsPerSample == 8;
        QImage::Format imageFormat = rgb ? QImage::Format_RGB888 : QImage::Format_ARGB32;

        QImage img = QImage(reinterpret_cast<const uchar*>(data.constData()), width, height, imageFormat);
        if (!rgb) img = img.rgbSwapped();

        return QPixmap::fromImage(img);
    }
    QPixmap getImageFromString(const QString& str) {
        QUrl url(str);
        auto file = url.toLocalFile();
        if (url.isValid() && QFile::exists(file))
            return QPixmap(file);
        return QIcon::fromTheme(str).pixmap(128);
    }

    QPixmap getImage(const QString& app_icon, const QVariantMap& hints) {
        if (auto& h = hints["image-data"]; !h.isNull()) return getImageFromHint(h);
        if (auto& h = hints["image-path"]; !h.isNull()) return getImageFromString(h.toString());
        if (!app_icon.isEmpty()) return getImageFromString(app_icon);
        if (auto& h = hints["icon_data"]; !h.isNull()) return getImageFromHint(h);
        return QPixmap();
    }
}

Notification::Notification(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Notification) {
    ui->setupUi(this);

    setWindowFlags(Qt::X11BypassWindowManagerHint | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    setAttribute(Qt::WA_X11NetWmWindowTypeNotification);
    setAttribute(Qt::WA_DeleteOnClose);

    setAttribute(Qt::WA_TranslucentBackground); // no background...
    ui->container->setBackgroundRole(QPalette::Window); // because the container frame acts as the entire chrome

    connect(ui->body, &QLabel::linkActivated, this, [&, this] (const QString& link) {
        linkActivated = true; // block actions
        QDesktopServices::openUrl(link);
    });

    timer = new QTimer();
    connect(timer, &QTimer::timeout, this, [this] {
        closeReason = 1;
        close();
    });

    // force layout to match width
    ui->container->setMinimumWidth(minimumWidth());
    ui->container->setMaximumWidth(maximumWidth());

    installEventFilter(this);
    ui->body->installEventFilter(this);
}

Notification::~Notification() {
    delete ui;
}

void Notification::closeEvent(QCloseEvent* e) {
    emit this->onClose(closeReason);
    this->QDialog::closeEvent(e);
}

bool Notification::eventFilter(QObject* o, QEvent* e) {
    if (e->type() == QEvent::MouseButtonRelease) {
        if (!qobject_cast<QPushButton*>(o)) { // if not a left click on a push button
            auto me = static_cast<QMouseEvent*>(e);
            if (!geometry().contains(me->screenPos().toPoint())) return true;
            closeReason = 2; // manual dismiss
            if (me->button() == Qt::LeftButton) triggerAction("default");
            close();
        }
    }
    return QDialog::eventFilter(o, e);
}

void Notification::triggerAction(const QString& id) {
    if (linkActivated) return; // links block default action
    emit onTriggerAction(id);
    closeReason = 2; // manual dismiss
    close();
}

void Notification::setDetails(const QString &app_name [[maybe_unused]], const QString &app_icon, const QString &summary, const QString &body, const QStringList &actions, const QVariantMap &hints, int expire_timeout) {
    ui->iconContainer->hide();
    if (auto px = getImage(app_icon, hints); !px.isNull()) {
        ui->iconContainer->show();
        ui->icon->setPixmap(px);
    }

    qDeleteAll(ui->buttons->children()); // clear buttons
    for (int i = 0; i < actions.count(); i += 2) {
        QString action = actions[i], desc = actions[i+1];
        if (action == "default") continue;
        auto b = new QPushButton();
        ui->buttons->addWidget(b);
        b->setText(desc);
        connect(b, &QPushButton::clicked, this, [this, action] {
            triggerAction(action);
        });
    }
    ui->buttons->addItem(new QSpacerItem(0, 0));
    ui->buttons->update();

    setProperty("app-name", app_name);

    int urgency = 1;
    if (auto h = hints["urgency"]; h.canConvert(QVariant::Int)) urgency = h.toInt();
    switch(urgency) {
    case 0:
        setProperty("urgency", "low");
        break;
    case 1:
        setProperty("urgency", "normal");
        break;
    case 2:
        setProperty("urgency", "critical");
        break;
    default:
        setProperty("urgency", "normal");
        break;
    }

    ui->summary->setText(summary);
    /*if (Qt::mightBeRichText(body)) ui->body->setTextFormat(Qt::RichText);
    else ui->body->setTextFormat(Qt::MarkdownText);*/
    ui->body->setTextFormat(Qt::RichText);
    ui->body->setTextInteractionFlags(Qt::LinksAccessibleByMouse);
    ui->body->setText(body);

    adjustSize();
    emit onShow();
    show();

    if (urgency == 2) timer->stop();
    else {
        if (expire_timeout >= 0) timer->start(expire_timeout);
        else timer->start(Config::defaultTimeout);
    }
}
