#include "aludel.h"

#include "config.h"
#include "notifications_adaptor.h"

#include <QApplication>
#include <QScreen>

#include <QDebug>

namespace {
    Aludel* inst_ptr = nullptr;
}

Aludel* Aludel::Instance() {
    if (inst_ptr == nullptr) inst_ptr = new Aludel();
    return inst_ptr;
}

Aludel::~Aludel() {

}

Aludel::Aludel(QObject *parent) : QObject(parent) {
    new NotificationsAdaptor(this);
    Config::init();
}

void Aludel::updateGeometry() {
    auto screen = QApplication::primaryScreen()->geometry();
    auto p = Config::cornerProportion();
    auto off = Config::cornerOffset();
    auto startPoint = screen.topLeft() + QPoint(screen.width() * p.x(), screen.height() * p.y()) - QPoint(Config::toastMargin.x() * off.x(), Config::toastMargin.y() * off.y());

    int cursor = 0;

    for (auto n : qAsConst(notifications)) {
        if (!n) continue;
        auto r = n->geometry();
        Config::moveRectCorner(r, startPoint + QPoint(0, cursor * off.y() * -1));
        n->move(r.topLeft());
        cursor += r.height() + Config::toastSpacing;
    }
}

QStringList Aludel::GetCapabilities() {
    QStringList c;
    c
            << QS("actions")
            // << QS("action-icons")
            << QS("body")
            << QS("body-markup")
            << QS("body-hyperlinks")
            << QS("body-images")
            // << QS("icon-multi")
            // << QS("icon-static")
            << QS("persistence")
            ;
    return c;
}

QString Aludel::GetServerInformation(QString &vendor, QString &version, QString &spec_version) {
    spec_version = QS("1.2");
    version = QS("r0");
    vendor = QS("zetaprime.net");
    return QS("aludel");
}

uint Aludel::Notify(const QString &app_name, uint replaces_id, const QString &app_icon, const QString &summary, const QString &body, const QStringList &actions, const QVariantMap &hints, int expire_timeout) {
    auto id = replaces_id;
    if (id == 0) {
        nextId++;
        id = nextId;
    }

    Notification* n = notifications[id];
    if (!n) {
        n = new Notification();
        notifications[id] = n;
        connect(n, &Notification::onTriggerAction, this, [&, id](const QString& action) {
            emit ActionInvoked(id, action);
        });
        connect(n, &Notification::onClose, this, [&, n, id] (uint reason) {
            if (notifications[id] == n) notifications.remove(id);
            emit NotificationClosed(id, reason);
            updateGeometry();
        });
        connect(n, &Notification::onShow, this, [&] { updateGeometry(); });
    }
    n->setDetails(app_name, app_icon, summary, body, actions, hints, expire_timeout);

    return id;
}

void Aludel::CloseNotification(uint id) {
    if (auto n = notifications[id]; n) {
        n->closeReason = 3; // closed by message
        n->close();
    }
}
