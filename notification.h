#pragma once

#include <QDialog>
#include <QTimer>

namespace Ui {
    class Notification;
}

class Notification : public QDialog {
    Q_OBJECT

public:
    explicit Notification(QWidget *parent = nullptr);
    ~Notification() override;

    void setDetails(const QString& app_name,
                    const QString& app_icon,
                    const QString& summary,
                    const QString& body,
                    const QStringList& actions,
                    const QVariantMap& hints,
                    int expire_timeout);

protected:
    void closeEvent(QCloseEvent*) override;
    //void mouseReleaseEvent(QMouseEvent*) override;
    bool eventFilter(QObject*, QEvent*) override;

private:
    Ui::Notification *ui;

    QTimer* timer;
    bool linkActivated = false;

    void triggerAction(const QString&);

public: // vars
    uint closeReason = 1;

signals:
    void onShow();
    void onClose(uint = 1);
    void onTriggerAction(const QString&);
};
