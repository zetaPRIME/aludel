QT += core gui dbus

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += $$files(*.cpp, true)
HEADERS += $$files(*.h, true) \
           $$files(*.hpp, true)
FORMS += $$files(*.ui, true)

#DBUS_ADAPTORS += org.freedesktop.Notifications.xml
#DBUS_INTERFACES += org.freedesktop.Notifications.xml

DISTFILES += .astylerc \

# Default rules for deployment.
linux: target.path = /usr/bin
else: unix:!android: target.path = /usr/local/bin
!isEmpty(target.path): INSTALLS += target
